<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('servings');
            $table->integer('cook');
            $table->integer('prep');
            $table->string('img_path');
            $table->string('notes');
            $table->text('description');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('category_id');
            
            $table->foreign('category_id')
            ->on('categories')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('user_id')
            ->on('users')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
