@extends('templates.template')

@section('assets')
<link rel="stylesheet" href="{{asset('css/forms.css')}}">
@endsection

@section('title', 'edit profile')
@section('content')

    <div class="col-lg-8 offset-lg-2 green">
            <div class="container-fluid mt-5">

            <div class="row d-flex justify-content-center">
                <h3 class ="title">Edit Profile</h3>
            </div>

            <div class="row ">
                <div class="col-sm-4 offset-sm-1 mt-2">
                    <form action="/editprofile/{{$profile->id}} " method="POST" enctype="multipart/form-data">
                    @csrf
                    @method("PATCH")
                    <div class="form-group ">
                        <label for="description">Description:</label>
                        <textarea name="description" class="form-control special h-10 ">{{$profile->description}} </textarea>
                    </div>
                    <div class="form-group">
                        <label for="img_path" height="300px">Profile Image:</label>
                        <input type="file" name="img_path" class="form-control h-10 rounded-pill" value="{{$profile->img_path}} ">
                    </div>
                    <div class="form-group">
                        <label for="twitter" height="300px">Twitter:</label>
                        <input type="text" name="twitter" class="form-control h-10 rounded-pill" value="{{$profile->twitter}} ">
                    </div>
                    <div class="form-group">
                        <label for="instagram" height="100px">Instagram:</label>
                        <input type="text" name="instagram" class="form-control h-10 rounded-pill" value="{{$profile->instagram}} ">
                    </div>
                    <div class="form-group">
                        <label for="facebook" height="300px">Facebook:</label>
                        <input type="text" name="facebook" class="form-control h-10 rounded-pill" value="{{$profile->facebook}} ">
                    </div>
                    <div class="form-group">
                        <label for="website" height="300px">Website</label>
                        <input type="text" name="website" class="form-control h-10 rounded-pill"
                        value="{{$profile->website}} ">
                    </div>
                    <div class="mb-5">
                        <button type="submit" class="rounded-pill butoon">Edit Profile</button>
                    </div>
                    
                    </form>
                    
                </div>
           
                <div class="col-sm-4 offset-sm-1">
                    <h4 class=" quote d-flex align-items-center">"No one is born a great cook, one learns by doing."</h4>
                    <h5 class="writer">- Julia Child</h5>
                </div>

            </div>

        </div>
    </div>

   


{{-- <div class="col-lg-5 offset-lg-2">
    <form action="/makeprofile" method="POST" enctype="multipart/form-data">
    @csrf
    @method("PATCH")
    <div class="form-group">
        <label for="description">Description:</label>
        <textarea name="description" class="form-control">Tell us about yourself!</textarea>
    </div>
     <div class="form-group">
        <label for="img_path" height="300px">Profile Image:</label>
        <input type="file" name="img_path" class="form-control">
    </div>
    <div class="form-group">
        <label for="twitter" height="300px">Twitter:</label>
        <input type="text" name="twitter" class="form-control">
    </div>
    <div class="form-group">
        <label for="instagram" height="300px">Instagram:</label>
        <input type="text" name="instagram" class="form-control">
    </div>
    <div class="form-group">
        <label for="facebook" height="300px">Facebook:</label>
        <input type="text" name="facebook" class="form-control">
    </div>
    <div class="form-group">
        <label for="website" height="300px">Website</label>
        <input type="text" name="website" class="form-control">
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create Profile</button>
    </div>
    
    </form> --}}

@endsection




 

