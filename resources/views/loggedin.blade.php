@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/allevents.css')}}">
@endsection
@section('content')
<div class="container ">
    <div class="row justify-content-center">
        <div class="col-md-8 ">

            <div class="row welcome d-flex text-center align-items-center">
                Welcome to Foodie, a recipe bank where nothing gets lost!
            </div>
           

                {{-- <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    {{-- @endif 

                   Welcome!
                </div> --}}
            </div>
            
           

            
        </div>
 <div class="row d-flex justify-content-around flex-wrap col-lg-8 offset-lg-2">
            @auth()
            @if(Auth::user()->role_id==1)
            
            <a href="/admin/addevent" class="butoon2">Create Event</a>
            <a href="/admin/addcategory" class="butoon2">Create Categories</a>
            <a href="/admin/userprofiles" class="butoon2">View Users</a>
            @else
           
      
            
           
            <a href="/makeprofile" class="butoon2">Create Profile</a>
           
            @endif
            @endauth
        </div>
    </div>
</div>
@endsection
