<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield("title")</title>
   
  @yield('assets')
    <!-- bootswatch -->
    {{-- <link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.css"> --}}
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<script src="{{asset('js/jquery.min.js')}}"></script>



<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
</head>

<body>
  <div class="col-lg-10 offset-lg-1">
 <nav class="navbar navbar-expand-lg navbar-trans d-flex sticky-top">
  <a class="" href="/profile">Foodie</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse"  aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse" id="">
    <ul class="navbar-nav ml-5">
     {{-- <li class="nav-item">
        <a class="nav-link" href="/catalog">home</a>
      </li> --}}

      

        @auth()
        <li class="nav-item  ml-4">
            <a class="nav-link" href="">hi {{Auth::user()->name}}</a>
        </li>
       
        @if(Auth::user()->role_id==1)
         <li class="nav-item  ml-4">
            <a class="nav-link" href="/admin/allcategories">categories</a>
          </li>
          <li class="nav-item ml-4">
            <a class="nav-link" href="/admin/addcategory">add category</a>
            </li>
           <li class="nav-item  ml-4">
          <a class="nav-link" href="/userallevents">events</a>
          </li>
          <li class="nav-item  ml-4">
          <a class="nav-link" href="/admin/addevent">add events</a>
          </li>
           <li class="nav-item ml-4">
            <a class="nav-link" href="/admin/userprofiles">profiles</a>
            </li>
            
          
            {{-- users --}}
        @else 
         <li class="nav-item  ml-4">
          <a class="nav-link" href="/profile">profile</a>
        </li>
        <li class="nav-item  ml-4">
          <a class="nav-link" href="/userallevents">events</a>
        </li>
        <li class="nav-item  ml-4">
          <a class="nav-link" href="/addrecipe">add recipe</a>
        </li>

      
        @endif
         <li class="nav-item ml-4 pull-right">
            {{-- logout will only work through the POST method --}}
            <form method="POST" action="/logout">
            @csrf
                <button type="submit" class="btn btn-success">Logout</button>
            </form>
        </li>
        
        @else
          <li class="nav-item  ml-4">
            <a class="nav-link" href="/login">Login</a>
          </li>
          <li class="nav-item  ml-4">
            <a class="nav-link" href="/register">Register</a>
          </li>
        @endauth


     
      
      
      
    </ul>
   
  </div>
  </div>
</nav>     




@yield("content")
    <footer class="fixed-bottom text-center">
        <div class="container sticky-bottom">
            <p class="text-center">This site was created with HTML, CSS/SASS, JS and PHP/Laravel</p>
        </div>
    </footer>
</body>
</html>