@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/allevents.css')}}">
@endsection

@section('title', 'all profiles')

@section('content')
<div class="col-lg-8 offset-lg-2" id="yellow" style="border: 8px solid rgba(159, 196, 136, 0.9);
    box-sizing: border-box; border-radius: 25px;">
    <div class="container-fluid mt-5">

    <div class="row d-flex justify-content-center">
        <h3 class ="title">User Profiles</h3>
    </div>

<div class="container">
    <div class="row">
        <div class="col-lg-10 ">
            <table class="table table-stripe border">
              <thead>
                <tr>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>
              </thead>
               <tbody>
                @foreach($users as $user)
               
               <tr>
                <td>{{$user->name}} </a></td>
                <td>{{$user->email}}</td>
                <td>{{$user->password}} </td>
                <td>
                    <a href="/admin/deleteuser/{{$user->id}}" class="butoon-red">delete</a></td>
                  
               </td>
               
                </tr>
               @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


    
 
  



