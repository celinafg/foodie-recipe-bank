@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/recipe.css')}}">
@endsection
@section('title', 'view recipe ')
@section('content')

 <section class="header" >
   <div class="container recipephoto" style="background-image: url('images/kitchen.jpg');">
       <div style="background-image: url('images/yes.png'); background-size:cover"></div>
   </div>
</section>

<div class="container">
    <div class="row">
        <h3 class="mt-5 ml-5">{{$recipe->title}} </h3>
    </div>

    <div class="row d-flex align-items-center justify-content-end">
        <p class="times">cook time: {{$recipe->cook}} </p>
        <p class="times">prep time: {{$recipe->prep}} </p>
        <p class="times">servings: {{$recipe->servings}} </p>
        <p class="times">category: {{$recipe->category->name}} </p>
    </div>
    <div class="row d-flex align-items-center justify-content-end mt-5 mr-3" style="max-height:100px;">
         <button type="submit" class="butoon mr-3 mb-3"><a href="/editrecipe/{{$recipe->id}} ">e</a></button>
          <form action="/deleterecipe/{{$recipe->id}} " method="POST">
            @csrf
            @method("DELETE")
            <button class="butoon red">d</button>
        </form>

    </div>


 <div class="container mt-5">
  <div class="row">
    <div class="col-sm-4 text-align-right separator ingredients">
      <h4 class="d-flex justify-content-end mr-4">ingredients</h4>
    </div>
    <div class="col-sm-8 ">
      <h4 class="ml-4">procedure</h4>
    </div>
  </div>
  
  {{-- foreach --}}
  
  <div class="row">
  
    <div class="col-sm-4 text-align-right separator ing mt-5">
    @foreach($ingredients as $ingredient)
      <h5 class="d-flex justify-content-end mr-4">{{$ingredient->name}} </h4>
      @endforeach
    </div>
    <div class="col-sm-8 mt-5 proc">
    @foreach($procedures as $procedure)
      <h5 class="ml-4">{{$procedure->name}} </h4>
      @endforeach
    </div>
  </div>
</div> 

    
    <div class="row mt-5">
        <div class="col-lg-8 offset-lg-2 proc">
            <h4>notes</h4>
            <h5 class>
                {{$recipe->notes}}
            </h5>
        </div>
    </div>

    <div class="d-flex align-items-center">
        <button>more recipes</button>
    </div>

</div>
{{-- <h2 class="text-center py-4">{{$recipe->name}} </h2>

<div class="container">
    <div class="row">

 
<a href="/viewrecipe/{{$recipe->id}} "></a>      
        <div class="col-lg-6">
            <h4>{{$recipe->title}} </h4>
            <h3>{{$recipe->description}} </h3>
            <p>{{$recipe->ingredients}} </p>
            <p>{{$recipe->procedure}} </p>
            <p>{{$recipe->servings}} </p>
            <p>{{$recipe->prep}} </p>
            <p>{{$recipe->cook}} </p>
            <img src="{{$recipe->img_path}}" alt="">
            <p>{{$recipe->notes}} </p>
            <p>{{$recipe->category->name}}</p>
        </div>
        <a href="/editrecipe/{{$recipe->id}}" class="btn btn-info">Edit Recipe</a>
        <form action="/deleterecipe/{{$recipe->id}} " method="POST">
            @csrf
            @method("DELETE")
            <button class="btn btn-danger">Delete Recipe</button>
        </form>
    </div> --}}

@endsection