@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/forms.css')}}">
@endsection
@section('title', 'edit recipe')
@section('content')


<div class="col-lg-8 offset-lg-2 green">
    <div class="container-fluid mt-5">

    <div class="row d-flex justify-content-center">
        <h3 class ="title">Edit Recipe</h3>
    </div>

    <div class="row ">
        <div class="col-sm-4 offset-sm-1 mt-2">
            <form action="editrecipe/{{$recipe->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method("PATCH")
            <div class="form-group ">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control h-10 rounded-pill" value="{{$recipe->title}} ">    
            </div>
                <div class="form-group ">
                <label for="description">Short Description</label>
                <textarea name="description" class="form-control special h-10 ">{{$recipe->description}}</textarea>
            </div>
            <div class="form-group ">
                <label for="ingredients">Ingredients</label>
                 <textarea name="ingredients" class="form-control special h-10 "  >{{$allingredients}}</textarea>    
            </div>
            <div class="form-group ">
                <label for="procedure">Procedure</label>
                {{-- foreach everthing! --}}
                <textarea name="procedure" class="form-control special h-10 ">{{$allprocedures}} </textarea>  
            </div>
            <div class="form-group ">
                <label for="servings">Servings</label>
                <input type="number" min="1" name="servings" class="form-control h-10 rounded-pill" value="{{$recipe->servings}} ">   
            </div>
            <div class="form-group ">
                <label for="prep">Preptime in Minutes</label>
                <input type="number" min="1" name="prep" class="form-control h-10 rounded-pill" value="{{$recipe->prep}} ">    
            </div>
             <div class="form-group ">
                <label for="cook">Cooktime in Minutes</label>
                <input type="number"  min="1" name="cook" class="form-control h-10 rounded-pill" value="{{$recipe->cook}} ">    
            </div>
            <div class="form-group">
                <label for="img_path" height="300px">Category Image:</label>
                <input type="file" name="img_path" class="form-control h-10 rounded-pill" value="{{$recipe->img_path}} ">
            </div>
            <div class="form-group ">
                <label for="notes">Recipe Notes</label>
                <input type="text" name="notes" class="form-control h-10 rounded-pill" value="{{$recipe->notes}}">    
            </div>
            <div class="form-group ">
                <label for="category_id">Category</label>
                 <select name="category_id" class="form-control">
                @foreach($categories as $category)
                <option value=" {{$category->id}} "> {{$category->name}} </option>
            @endforeach
        </select>
            </div>


            <div class="mb-5">
                <button type="submit" class="rounded-pill butoon">Edit Recipe</button>
            </div>
            </form>
    
        </div>
        <div class="col-sm-4 offset-sm-1">
            <h4 class=" quote d-flex align-items-center">"No one is born a great cook, one learns by doing."</h4>
            <h5 class="writer">- Julia Child</h5>
        </div>
    </div>








{{--

 <h2 class="text-center py-4">Edit Recipe</h2>
<div class="col-lg-4 offset-lg-1">
    <form action="/editrecipe/{{$recipe->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method("PATCH")
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" class="form-control" value="{{$recipe->title}} ">
    </div>
    <div class="form-group">
        <label for="description">Short Description</label>
        <textarea name="description" class="form-control">{{$recipe->description}}</textarea>
    </div>
    <div class="form-group">
        <label for="ingredients">Ingredients</label>
        <textarea name="ingredients" class="form-control h-10 rounded-pill">{{$recipe->ingredients}}</textarea>
    </div>
    <div class="form-group">
        <label for="procedure">Procedure</label>
        <textarea name="procedure" class="form-control">{{$recipe->procedure}}</textarea>
    </div>
     <div class="form-group">
        <label for="servings">Servings</label>
        <input type="number" name="servings" class="form-control" value="{{$recipe->servings}} ">
    </div>
     <div class="form-group">
        <label for="prep">Preptime in Minutes</label>
        <input type="number" name="prep" class="form-control" value="{{$recipe->prep}} ">
    </div>
     <div class="form-group">
        <label for="cook">Cooktime in Minutes</label>
        <input type="number" name="cook" class="form-control" value="{{$recipe->cook}} ">
    </div>
     <div class="form-group">
        <label for="img_path" height="300px">Category Image:</label>
        <input type="file" name="img_path" class="form-control">
    </div>
    <div class="form-group">
        <label for="notes">Recipe Notes</label>
        <textarea name="notes" class="form-control">{{$recipe->notes}} </textarea>
    </div>
    <div class="form-group">
        <label for="category_id">Recipe Category:</label>
        <select name="category_id" class="form-control">
            @foreach($categories as $category)
            <option value=" {{$category->id}} "> {{$category->name}} </option>
            @endforeach
        </select>
    </div>

        <button type="submit" class="btn btn-primary">edit Recipe</button>
    </form>
</div>
--}}




@endsection