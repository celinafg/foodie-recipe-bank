@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/categories.css')}}">
@endsection

@section('title', 'all categories')
@section('content')
<h3 class="d-flex justify-content-center">Categories</h3>

<div class="row">
    <div class="col-lg-2 offset-lg-1 mt-5">
        <form action="/allcategories" method="POST">
        @csrf
        <select name="category_id" class="form-control">
            <option value="0">All</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}"
                    
                    @if(isset($catId))
                        @if($catId== $category->id)
                        selected
                        @endif
                    @endif
                >{{$category->name}}</option>
            @endforeach
        </select>
        <button class="btn btn-success" type="submit">Filter</button>
        </form>
    </div>
</div>

<div class="container">
  <div class="row">
    @foreach($categories as $category)
    <div class="col-lg-4 mt-5">
      <div class="card" style="border-radius:30px;">
        <img src="{{asset($category->img_path)}}" height="300px" class="card-img-top" style="height:300px; border-radius:30px;">
        <div class="card-body">
          <h5 class="card-title ml-3"> {{$category->name}} </h5>
          <p class="card-text ml-3">{{$category->description}} </p>
          <p class="card-text ml-3"><small class="text-muted">{{$category->created_at->diffForHumans()}} </small></p>
        </div>
        
        <div class="card-footer d-flex justify-content-between flex-wrap">
        <a href="/admin/editcategory/{{$category->id}}" class="butoon2">Edit Category</a>
        <form action="/admin/deletecategory" method="POST">
          @csrf
          @method("DELETE")
          <input type="hidden" name="id" value="{{$category->id}}">
          <button type="submit" class="butoon-red">Delete</button>
        </form>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>



    
 
  


@endsection

