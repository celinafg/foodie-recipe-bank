@extends('templates.template')

@section('assets')
<link rel="stylesheet" href="{{asset('css/forms.css')}}">
@endsection

@section('title', 'edit categories')
@section('content')

<div class="col-lg-8 offset-lg-2 green">
            <div class="container-fluid mt-5">

            <div class="row d-flex justify-content-center">
                <h3 class ="title">Edit Category</h3>
            </div>

            <div class="row ">
                <div class="col-sm-4 offset-sm-1 mt-2">
                         <form action="/admin/editcategory/{{$category->id}}  " method="POST" enctype="multipart/form-data">
                        @csrf
                        @method("PATCH")
                            <div class="form-group ">
                                <label for="categoryname">Category Name:</label>
                             <input type="text" name="categoryname" class="form-control h-10 rounded-pill" value="{{$category->name}} ">    
                            </div>
                             <div class="form-group ">
                                <label for="description">Description:</label>
                                <textarea name="description" class="form-control special h-10 ">{{$category->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="img_path" height="100px">Category Image:</label>
                                <input type="file" name="img_path" class="form-control h-10 rounded-pill" value="{{$category->img_path}}">
                            </div>
                            <div class="mb-5">
                                <button type="submit" class="rounded-pill butoon">Edit Category</button>
                            </div>
                            
                            
                            </form>
                    
                </div>
                <div class="col-sm-4 offset-sm-1">
                    <h4 class=" quote d-flex align-items-center">"No one is born a great cook, one learns by doing."</h4>
                    <h5 class="writer">- Julia Child</h5>
                </div>
            </div>

            </div>

{{--  
<div class="col-lg-4 offset-lg-1">
    <form action="/admin/editcategory/{{$category->id}}  " method="POST" enctype="multipart/form-data">
    @csrf
    @method("PATCH")
    <div class="form-group">
        <label for="categoryname">Category Name</label>
        <input type="text" name="categoryname" class="form-control" value="{{$category->name}} ">
    </div>
    <div class="form-group">
        <label for="description">Description:</label>
        <textarea name="description" class="form-control" value="">{{$category->description}}</textarea>
    </div>
     <div class="form-group">
        <label for="img_path" height="300px">Category Image:</label>
        <input type="file" name="img_path" class="form-control">
    </div>

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">edit Category</button>
    </div>
    
    </form>
</div>--}}
@endsection




 

{{-- show added category 
    <div class="col-lg-4 offset-lg-1">
        <table class="table table-striped">
            <thead>
                
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Created at</th>
                    <th></th> {{-- number of recipes under this category
                </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->name}}</td>
                    <td>{{$category->description}}</td>
                    <td>{{$category->created_at->diffForHumans()}}</td>
                    <td></td> {{--number of recipes under this category
                </tr>
            @endforeach 
            </tbody>
        </table>
    </div>
 --}}
