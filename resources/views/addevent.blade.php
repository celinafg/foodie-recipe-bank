@extends('templates.template')

@section('assets')
<link rel="stylesheet" href="{{asset('css/forms.css')}}">
@endsection
@section('title', 'add event')
@section('content')

<div class="col-lg-8 offset-lg-2 green">
    <div class="container-fluid mt-5">

    <div class="row d-flex justify-content-center">
        <h3 class ="title">Create Event</h3>
    </div>

    <div class="row ">
        <div class="col-sm-4 offset-sm-1 mt-2">
                    <form action="/admin/addevent" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group ">
                        <label for="name">Event Name:</label>
                        <input type="text" name="name" class="form-control h-10 rounded-pill">    
                    </div>
                        <div class="form-group ">
                        <label for="description">Description:</label>
                        <textarea name="description" class="form-control special h-10 "></textarea>
                    </div>
                    <div class="form-group ">
                        <label for="seats">Event Seats:</label>
                        <input type="number" min="1" name="seats" class="form-control h-10 rounded-pill">    
                    </div>
                    <div class="form-group ">
                        <label for="date">Date:</label>
                        <input type="date" name="date" class="form-control h-10 rounded-pill">    
                    </div>
                    
             
                    <div class="mb-5">
                        <button type="submit" class="rounded-pill butoon">Create Event</button>
                    </div>
                    
                    </form>
            
        </div>
        <div class="col-sm-4 offset-sm-1">
            <h4 class=" quote d-flex align-items-center">"No one is born a great cook, one learns by doing."</h4>
            <h5 class="writer">- Julia Child</h5>
        </div>
    </div>

    </div>




{{--  
 <h2 class="text-center py-4">Add Event</h2>
<div class="col-lg-4 offset-lg-1">
    <form action="/admin/addevent " method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="name">Event Name</label>
        <input type="text" name="name" class="form-control" value="">
    </div>
    <div class="form-group">
        <label for="description">Description:</label>
        <textarea name="description" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <label for="seats">Event Seats:</label>
       <input type="number" name="seats" class="form-control">
    </div>
     <div class="form-group">
        <label for="date" height="300px">Date:</label>
        <input type="date" name="date" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Create Event</button>
    </form>
</div>--}}




@endsection