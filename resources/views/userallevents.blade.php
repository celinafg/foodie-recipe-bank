@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/allevents.css')}}">
@endsection
@section('title', 'events')
@section('content')

<div class="col-lg-8 offset-lg-2" id="yellow" style="border: 8px solid rgba(159, 196, 136, 0.9);
    box-sizing: border-box; border-radius: 25px;">
    <div class="container-fluid mt-5">

    <div class="row d-flex justify-content-center">
        <h3 class ="title" style="">Upcoming Events</h3>
    </div>

	 <div class="container">
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <table class="table table-stripe border">
              <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Event Description</th>
                    <th>Event Seats</th>
                    <th>Event Date</th>
                    <th>Action</th>
                    <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($events as $event)
               <tr>
                 <td><a href="/viewevent/{{$event->id}}">{{$event->name}} </a></td>
                <td>{{$event->description}}</td>
                <td><span style="color:red" id="seats_{{$event->id}}">{{$event->seats}} </span></td>
                <td>{{$event->date}}</td>
                
                    @auth
                    @if(Auth::user()->role_id==1)
                    <td>
                    <a href="/admin/editevent/{{$event->id}}" class="butoon">edit</a></td>
                    <td>
                     <a href="/admin/deleteevent/{{$event->id}}" class="butoon-red">delete</a></td>
                    @else
                    <td>
                    <button class="butoon" id="disableBtn" onclick="attendevent({{$event->id}});">Attend</button>
                   </td>
                    @endif
                    @endauth
                     
               
                </tr>
               @endforeach
              </tbody>
            </table>
        </div>
    </div>
    
</div>
    </div>
</div>

    @auth 
    @if(Auth::user()->role_id==1)
    
    @else
        <div class="row">
        <a href="/showmyevents" class="col-lg-3 offset-lg-2"><h3>My Events</h3></a>
    </div>
    @endif
    @endauth
   

		<script type="text/javascript">
			function attendevent(id){
				let data = new FormData;

				data.append('_token', "{{csrf_token () }}"); 
				data.append('id', id);

				fetch('/attendevent', {
					method: "POST",
					body: data
				}).then(response=>{
					return response.text();
				}).then(data=>{
                    document.querySelector('#seats_'+id).innerHTML = `<strong>${data}</strong>`;
                });
                
                document.getElementById("disableBtn").disabled = true
            }
          
		</script>


@endsection