@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/allevents.css')}}">
@endsection
@section('title', 'my events')
@section('content')


<div class="col-lg-8 offset-lg-2" id="yellow" style="border: 8px solid rgba(159, 196, 136, 0.9);
    box-sizing: border-box; border-radius: 25px;">
    <div class="container-fluid mt-5">

    <div class="row d-flex justify-content-center">
        <h3 class ="title">My Events - User</h3>
    </div>

<div class="container">
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <table class="table table-stripe border">
              <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Event Description</th>
                    <th>Event Seats</th>
                    <th>Event Date</th>
                    <th>Action</th>
                    <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($user->events as $event)
               <tr>
                 <td><a href="/viewevent/{{$event->id}}">{{$event->name}} </a></td>
                <td>{{$event->description}}</td>
                <td>{{$event->seats}} </td>
                <td>{{$event->date}}</td>
                <td>
                    <form action="/cancelreservation" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="event_id" value="{{$event->id}} ">
                    <button class="butoon-red">Cancel Attendance</button>
                </form>
               </td>
                </tr>
               @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

	 {{-- <div class="row col-lg-10 offset-lg-1 mt-4">
        <div class="table table-striped d-flex justify-content-center ">
          
          <table>
            <thead>
                <th>Event Name</th>
                <th>Event Description</th>
                <th>Event Seats</th>
                <th>Event Date</th>
                <th>Action</th>
                <th></th>
                
            </thead>
            <tbody class="mt-5">
           <div class="row">
            <tr>
                  <tbody>
                 @foreach($user->events as $event)
               <tr>
                 <td><a href="/viewevent/{{$event->id}}">{{$event->name}} </a></td>
                <td>{{$event->description}}</td>
                <td>{{$event->seats}} </td>
                <td>{{$event->date}}</td>
                <td>
                    <form action="/cancelreservation" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="event_id" value="{{$event->id}} ">
                    <button class="btn btn-danger">Cancel Attendance</button>
                </form>
               </td>
                </tr>
               @endforeach
              </tbody>  
            </tr>
           </div>
            </tbody>
         
        </table>
    </div>
</div>
	</div>
</div>

    --}}




{{--  
<h1 class="text-center py-5">My Events</h1>

<div class="col-lg-4 offset-lg-4">
    <ul>
       @foreach($user->events as $event)
        <li>{{$event->name}}<br> 
        <form action="/cancelreservation" method="POST">
            @csrf
            @method('DELETE')
            <input type="hidden" name="event_id" value="{{$event->id}} ">
            <button class="btn btn-danger">Cancel Attendance</button>
        </form>
        </li>
        @endforeach
    </ul>
</div>


{{--  @foreach($user->events as $event)
        <li>{{$event->name}}<br> 
        <form action="/cancelreservation" method="POST">
            @csrf
            @method('DELETE')
            <input type="hidden" name="event_id" value="{{$event_id}} ">
            <button class="btn btn-danger">Cancel Attendance</button> --}}
