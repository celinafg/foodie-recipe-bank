@extends('templates.template')

@section('assets')
<link rel="stylesheet" href="{{asset('css/allevents.css')}}">
@endsection
@section('title', 'view event')
@section('content')

 <div class="col-lg-8 offset-lg-2" id="yellow" style="border: 8px solid rgba(159, 196, 136, 0.9);
    box-sizing: border-box; border-radius: 25px;">
    <div class="container-fluid mt-5">

    <div class="row d-flex justify-content-center">
        <h3 class ="title" style="">{{$event->name}} </h3>
    </div>

    <div class="row col-sm-10 offset-sm-1 mr-5 mb-5">
       <a href="/viewevent/{{$event->id}}"></a>      
        <div class="col-lg-6">
            <h4><span class="title2">name: </span><span class="for">{{$event->name}} </span></h4>
            <h5><span class="title2">description:</span><span class="for">{{$event->description}}</span> </h5>
            <p><span class="title2">date: </span><span class="for">{{$event->date}}</span> </p>
            <p><span class="title2">seats:</span><span class="for">{{$event->seats}}</span> </p>
            <p><span class="title2">created on: </span><span class="for">{{$event->created_at->diffForHumans()}}</span> </p>
          
        
        </div>
        
        {{-- <a href="/editrecipe/{{$recipe->id}}" class="btn btn-info">Edit Recipe</a>
        <form action="/deleterecipe/{{$recipe->id}} " method="POST">
            @csrf
            @method("DELETE")
            <button class="btn btn-danger">Delete Recipe</button>
        </form> --}}
    </div>
    </div>
</div>
</div>
   

{{-- 

 
<a href="/viewevent/{{$event->id}}"></a>      
        <div class="col-lg-6">
            <h4>{{$event->name}} </h4>
            <h3>{{$event->description}} </h3>
            <p>{{$event->date}} </p>
            <p>{{$event->seats}} </p>
            <p>{{$event->created_at->diffForHumans()}} </p>
            <p>{{$event->updated_at->diffForHumans()}} </p>
        
        </div>
        {{-- <a href="/editrecipe/{{$recipe->id}}" class="btn btn-info">Edit Recipe</a>
        <form action="/deleterecipe/{{$recipe->id}} " method="POST">
            @csrf
            @method("DELETE")
            <button class="btn btn-danger">Delete Recipe</button>
        </form> --}}
    
@endsection