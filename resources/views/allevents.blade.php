@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/allevents.css')}}">
@endsection
@section('title', 'all events')
@section('content')
<h2 class="text-center py-4">Events</h2>

<div class="col-lg-8 offset-lg-2" id="yellow" style="border: 8px solid rgba(159, 196, 136, 0.9);
    box-sizing: border-box; border-radius: 25px;">
    <div class="container-fluid mt-5">

    <div class="row d-flex justify-content-center">
        <h3 class ="title">Upcoming Events - User</h3>
    </div>

<div class="container">
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <table class="table table-stripe border">
              <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Event Description</th>
                    <th>Event Seats</th>
                    <th>Event Date</th>
                    <th>Action</th>
                    <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($events as $event)
               <tr>
                 <td><a href="/viewevent/{{$event->id}}">{{$event->name}} </a></td>
                <td>{{$event->description}}</td>
                <td>{{$event->seats}} </td>
                <td>{{$event->date}}</td>
                <td>
            
                    <a href="/admin/editevent/{{$event->id}}" class="butoon">edit</a>
                     <a href="/admin/deleteevent/{{$event->id}}" class="butoon-red">delete</a>
               </td>
               
                </tr>
               @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

{{-- <div class="container">
  <div class="row">
    
    <div class="col-lg-10 offset-lg-1" >
      <div class="table table-striped">
        <table>
            <thead>
                <th>Event Name</th>
                <th>Event Description</th>
                <th>Event Seats</th>
                <th>Event Date</th>
                <th>Action</th>
                <th></th>
                
            </thead>
            <tbody>
            @foreach($events as $event)
            <tr>
                <td><a href="/viewevent/{{$event->id}}">{{$event->name}} </a></td>
                <td>{{$event->description}}</td>
                <td>{{$event->seats}} </td>
                <td>{{$event->date}}</td>
                
                <td> 
                    <a href="/admin/editevent/{{$event->id}}" class="btn btn-info">Edit Event</a>
                </td>
                <td>
                    <a href="/admin/deleteevent/{{$event->id}}" class="btn btn-info">Delete Event</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
       
  </div>
</div> --}}



    
 
  



