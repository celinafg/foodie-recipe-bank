
@extends('templates.template')
@section('assets')
<link rel="stylesheet" href="{{asset('css/profile.css')}}">
@endsection
@section('title', 'my profile')

@section('content')

<section class="header" >
    <div class="profile" id="profile">
        <div class="col-lg-10 offset-lg-1 d-flex h-25 rounded" id="rounded" style="background-image: url('images/yes.png'); background-size:cover"> 

        <div class="col-lg-2 rounded d-flex justify-content-center align-items-center" id="rounded3">
            <img src="{{$profile->img_path}} " alt="" class="rounded-circle border border-white" >
        </div>
        
        <div class="col-lg-6">
            <h3 class="mtt">{{$user->name}} </h3>
            <p class="col-lg-8 p-1">{{$profile->description}}  </p>
        </div>

        
    </div>
</section>
<div class="row">
    <div class="col-lg-2 offset-lg-1 mt-5">
        <form action="/profile" method="POST">
        @csrf
        <select name="category_id" class="form-control">
            <option value="0">All</option>
            @foreach($category as $indivcat)
                <option value="{{$indivcat->id}}"
                    
                    @if(isset($catId))
                        @if($catId== $indivcat->id)
                        selected
                        @endif
                    @endif
                >{{$indivcat->name}}</option>
            @endforeach
        </select>
        <button class="btn btn-success" type="submit">Filter</button>
        </form>
    </div>
</div>

<div class="container col-lg-10 offset-lg-1 mt-5">
    <a href="/addrecipe" class="create">Create Recipe</a>
    <a href="/editprofile/{{$profile->id}}" class="create">Edit Profile</a>
</div>

<section class="recipes">  
    <div class="row col-lg-10 offset-lg-1 h-100 d-flex justify-content-around  mtt2" >
        @foreach($recipes as $recipe) 
        <div style="min-width: 48%; max:width: 60%; ">
            <div class="col-sm conthit width d-flex roundedleft">
                <div class="col-sm-4 d-flex align-items-center ml-5" id="rounded2">
                    <img src="{{$recipe->img_path}} " class="rounded-circle border border-white" alt="">
                </div>
                <div class="mtt3 col-lg-7">
                    <h3 class=""><a href="/viewrecipe/{{$recipe->id}}"> {{$recipe->title}}</a></h3>
                    <p class="p-1 mtt4">{{$recipe->description}} </p>
                    <p class="p-1 mtt2"><small class="text-muted">{{$recipe->updated_at->diffForHumans()}}</small></p>
                </div>
                
            </div>  
        </div>
        @endforeach
    </div>
    
</div>



@endsection
