<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    public function category() {
        return $this->belongsTo('\App\Category');
    }

    public function profiles(){
        return $this->belongsTo('\App\Profile');
    }
}
