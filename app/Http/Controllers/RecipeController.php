<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;
use App\Category;
use Auth;
use App\User;
use App\Ingredient;
use App\Procedure;


class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        
        return view('addrecipe', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pls(Request $request)
    {

            $rules = array(
            "title" => "required", 
            "description"=>"required",
            // "ingredient_id" => "required", 
            // "procedure_id" =>"required",
            "servings"=>"required|numeric",
            "prep"=>"required|numeric",
            "cook"=>"required|numeric",
            "img_path" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "notes"=>"required",
            "category_id"=>"required"
            
        );

        $this->validate($request, $rules);
        $new_recipe = new Recipe;
        $new_recipe->title=$request->title;
        $new_recipe->description=$request->description;
       
        $new_recipe->servings=$request->servings;
        $new_recipe->prep=$request->prep;
        $new_recipe->cook=$request->cook;
        
        
        $image = $request->file("img_path");
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $new_recipe->img_path=$destination.$image_name;

        $new_recipe->notes=$request->notes;
        $new_recipe->category_id=$request->category_id;
        $new_recipe->user_id=Auth::user()->id;
       
        $new_recipe->save();
        


        $new_ing=$request->ingredients;
        $new_ing = explode(",", $new_ing);

        // dd($new_ing);
        foreach($new_ing as $indiv_ing){

            // dump($indiv_ing);
            $new_indiv_ing=new Ingredient;
            $new_indiv_ing->name = $indiv_ing;
            $new_indiv_ing->recipe_id=$new_recipe->id;
            $new_indiv_ing->save();
        }
        

        $new_proc=$request->procedure;
        $new_proc = explode(",", $new_proc);
        
        // dd($indiv_proc);
        foreach($new_proc as $indiv_proc){
            $new_indiv_proc=new Procedure;
            $new_indiv_proc->name = $indiv_proc;
            $new_indiv_proc->recipe_id=$new_recipe->id;
            $new_indiv_proc->save();
        }
    
        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe, $id)
    {
        $recipe=Recipe::find($id);
        $category=Category::all();
        $ingredients=Ingredient::where('recipe_id', $id)->get();
        
        $procedures=Procedure::where('recipe_id', $id)->get();
        return view('viewrecipe', compact('recipe', 'category', 'ingredients', 'procedures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe, $id)
    {
        $recipe=Recipe::find($id);
        $categories=Category::all();
        $ingredients=array(Ingredient::where('recipe_id', $id)->get());
        $allingredients = implode(", ", $ingredients);
        dd($allingredients);
        $procedures=array(Procedure::where('recipe_id', $id)->get());
        $allprocedures= implode(",", $procedures);
        return view("editrecipe", compact('categories', 'recipe', 'allingredients', 'allprocedures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $recipe=Recipe::find($id);
         $rules = array(
            "title" => "required", 
            "description"=>"required",
            // "ingredient_id" => "required", 
            // "procedure_id" =>"required",
            "servings"=>"nullable|required|numeric",
            "prep"=>"nullable|required|numeric",
            "cook"=>"nulable|required|numeric",
            "img_path" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "notes"=>"required",
            "category_id"=>"required"
            
        );

        $this->validate($request, $rules);
       
        $recipe->title=$request->title;
        $recipe->description=$request->description;
       
        $recipe->servings=$request->servings;
        $recipe->prep=$request->prep;
        $recipe->cook=$request->cook;
        
        
        if($request->file('img_path')!= null){
        $image = $request->file("img_path");
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $recipe->img_path=$destination.$image_name;

        $recipe->notes=$request->notes;
        $recipe->category_id=$request->category_id;
        $recipe->user_id=Auth::user()->id;
        }
        $recipe->save();
        


        $ing=$request->ingredients;
        $ing = explode(",", $ing);

        // dd($new_ing);
        foreach($ing as $indiv_ing){

            // dump($indiv_ing);
            
            $indiv_ing->name = $indiv_ing;
            $indiv_ing->recipe_id=$recipe->id;
            $indiv_ing->save();
        }
        

        $proc=$request->procedure;
        $proc = explode(",", $proc);
        
        // dd($indiv_proc);
        foreach($proc as $indiv_proc){
           
            $indiv_proc->name = $indiv_proc;
            $indiv_proc->recipe_id=$recipe->id;
            $indiv_proc->save();
        }
    
        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe, Request $request)
    {
        $recipe=Recipe::find($request->id);
        $recipe->delete();
        return redirect('/profile');
    }
}
