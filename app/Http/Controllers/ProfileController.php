<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Category;
use App\Recipe;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $req)
    {
        
                $recipes=Recipe::where('user_id', Auth::user()->id)->get();
                $category=Category::all();
                $user=User::find(Auth::user()->id);
                $profile=Profile::where('user_id', Auth::user()->id)->first();
            
                return view('profile', compact('profile', 'category', 'user', 'recipes'));
    }

    public function filter(Request $req) {
        if(isset($req->category_id) && $req->category_id>0){
            $category = Category::all();
            $recipes=Recipe::where('category_id', $req->category_id)->where('user_id', Auth::user()->id)->get();
            $catId = $req->category_id;
            $profile=Profile::where('user_id', Auth::user()->id)->first();
             $user=User::find(Auth::user()->id);
            return view('profile', compact("catId","category", "recipes", "profile", "user"));
        } else {
                $recipes=Recipe::where('user_id', Auth::user()->id)->get();
                $category=Category::all();
                $user=User::find(Auth::user()->id);
                $catId = $req->category_id;
                $profile=Profile::where('user_id', Auth::user()->id)->first();
                $recipes=Recipe::where('user_id', Auth::user()->id)->get();
                return view('profile', compact('profile', 'category', 'catId', 'user', 'recipes'));
        }
    }
    public function loggedin()
    {
        $user=User::find(Auth::user()->id);
        return view('/loggedin', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('makeprofile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = array(
            "description" => "required", 
            "twitter" => "required", 
            "instagram" => "required", 
            "facebook" => "required", 
            "website"=>"required", 
            "img_path" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048"
        );
        $this->validate($request, $rules);
        $new_profile = new Profile;
         $new_profile->user_id=Auth::user()->id;
        $new_profile->description=$request->description;
        $new_profile->twitter=$request->twitter;
        $new_profile->instagram=$request->instagram;
        $new_profile->facebook=$request->facebook;
        $new_profile->website=$request->website;
        $new_profile->img_path=$request->img_path;
       
        

        $image = $request->file("img_path");
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $new_profile->img_path=$destination.$image_name;
       
        $new_profile->save();
        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function showusers(Profile $profile)
    {
      $users=User::all();
      $profiles=Profile::all();  
      return view ('/userprofiles', compact('users', 'profiles'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile=Profile::find($id);
        return view('/editprofile', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile=Profile::find($id);
         $rules = array(
            "description" => "required", 
            "twitter" => "required", 
            "instagram" => "required", 
            "facebook" => "required", 
            "website"=>"required", 
            "img_path" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048"
        );
        $this->validate($request, $rules);
        $profile->user_id=Auth::user()->id;
        $profile->description=$request->description;
        $profile->facebook=$request->facebook;
        $profile->instagram=$request->instagram;
        $profile->twitter=$request->twitter;
        $profile->website=$request->website;

        if($request->file('img_path')!= null){
        $profile->img_path=$request->img_path;
        $image = $request->file("img_path");
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $profile->img_path=$destination.$image_name;
        }

        $profile->save();
        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user=User::find($request->id);
        $user->delete();
        return redirect('/admin/userprofiles');
    }

   
}
