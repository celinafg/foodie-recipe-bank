<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use App\User;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::all();
        
        return view('events', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addevent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = array(
            "name" => "required",
            "seats"=>"required|numeric|min:1",
            "description"=>"required",
            "date"=>"date_format:Y-m-d|after:today"
        );
        
        $this->validate($request, $rules);
        
        $new_event= new Event;
        $new_event->name=$request->name;
        $new_event->seats=$request->seats;
        $new_event->description=$request->description;
        $new_event->date=$request->date;
      
        $new_event->save();
        return redirect('/userallevents');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $events=Event::all();
        return view('/allevents', compact('events'));
    }

    public function viewevent($id){
      $event = Event::find($id);  
        return view('/viewevent', compact('event'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event, $id)
    {
        $event=Event::find($id);
        return view('/editevent', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $event=Event::find($id);
        
        $rules = array(
            "name" => "required",
            "seats"=>"required|numeric",
            "description"=>"required",
            "date"=>"date_format:d-m-Y|after:today"
        );
        
        // $this->validate($request, $rules);
        
        $event->name=$request->name;
        $event->seats=$request->seats;
        $event->description=$request->description;
        $event->date=$request->date;
        
        $event->save();
        return redirect('/userallevents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event, Request $request)
    {
        $event=Event::find($request->id);
        $event->delete();
        return redirect('/allevents');
    }

    public function userallevents(){
        $events=Event::all();
        return view('/userallevents', compact('events'));
    }

    // click attending on userevents
     public function attendevent(Request $request)
    {
        $event = Event::find($request->id);
        $event->users()->attach(Auth::user()->id);
        $event->seats-=1;
        $event->save();
        return ($event->seats);
    }


   
    // show user events
    public function showevents(){
        $user=User::find(Auth::user()->id);
        return view('/showmyevents', compact('user'));
    }

    public function cancelreservation(Request $request){
        $user=User::find(Auth::user()->id);

        $user->events()->detach($request->event_id);

        $event=Event::find($request->event_id);
        $event->seats +=1;
        $event->save();

        return redirect('/showmyevents');
    }

    
    

}
