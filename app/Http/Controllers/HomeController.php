<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Profile;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user=User::find(Auth::user()->id);
       
       
        
        return view('/loggedin', compact('user'));
        
    }

    public function showprofile(){
         $profile=Profile::where('user_id', Auth::user()->id)->count();
         return view('/loggedin', compact('profile'));
    } 
    
}
