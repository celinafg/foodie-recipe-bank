<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $categories = Category::all();
    //     return view('/allcategories', compact('categories'));
    // }

     public function index(Request $req) {
        if(isset($req->id) && $req->id>0){
            $categories = Category::all();
            $catId = $req->id;
             $allcategory = Category::where('id', $req->id)->get();
             dd($catId);
            return view('/allcategories', compact("catId","categories", 'allcategory'));
        
        } else{
            $categories = Category::all();
        return view('/allcategories', compact('categories'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('addcategory', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            "categoryname" => "required", 
            "description" => "required", 
            "img_path" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048"
        );

        $this->validate($request, $rules);
        $new_category = new Category;
        $new_category->name = $request->categoryname;
        $new_category->description = $request->description;
        $new_category->img_path=$request->img_path;
        $image = $request->file("img_path");
        $image_name = time().".".$image->getClientOriginalExtension();
        
        $destination = "images/";

        $image->move($destination, $image_name);
        $new_category->img_path=$destination.$image_name;

        $new_category-> save();

        return redirect('/admin/allcategories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::find($id);
        return view('editcategory', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $category=Category::find($id);
        
       $rules = array(
            "categoryname" => "required", 
            "description" => "required", 
            "img_path" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048"
       );

        $this->validate($request, $rules);
        $category->name = $request->categoryname;
        $category->description = $request->description;
       
       if($request->file('img_path')!= null){
        $image=$request->file('img_path');
        $image_name=time().".".$image->getClientOriginalExtension();
        $destination="images/";
        $image->move($destination, $image_name);
        $category->img_path=$destination.$image_name;
       }
       $category->save();
       
       return redirect('/admin/allcategories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $category=Category::find($request->id);
        $category->delete();
        return redirect('/admin/allcategories');
    }
}
