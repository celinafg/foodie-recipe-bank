<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function events(){
        return $this->belongsToMany('\App\Event'
    );
    }
    public function destroy(Request $request){
        $user=User::find($request->id);
        $user->delete();
        return redirect('/admin/userprofiles');
          
    }
}
