<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('landing');
});

Auth::routes();

Route::middleware("admin")->group(function(){ 
//admin - categories
Route::get('/admin/allcategories', 'CategoryController@index');
Route::post('/admin/allcategories', 'CategoryController@index');
Route::get('/admin/addcategory', 'CategoryController@create');
Route::post('/admin/addcategory', 'CategoryController@store');
Route::get('/admin/editcategory/{id}', 'CategoryController@edit');
Route::patch('/admin/editcategory/{id}', 'CategoryController@update');
Route::delete('/admin/deletecategory', 'CategoryController@destroy');

//admin -event
Route::get('/admin/addevent', 'EventController@create');
Route::post('/admin/addevent', 'EventController@store');
Route::get('/allevents', 'EventController@show');
Route::get('/admin/editevent/{id}', 'EventController@edit');
Route::patch('/admin/editevent/{id}', 'EventController@update');
Route::get('/admin/deleteevent/{id}', 'EventController@destroy');

//admin - profile
Route::get('/admin/userprofiles', 'ProfileController@showusers');
Route::get('/admin/deleteuser/{id}', 'ProfileController@destroy');
});

Route::middleware("user")->group(function(){ 
//user-event
Route::post("/attendevent", "EventController@attendevent");
Route::get('showmyevents', 'EventController@showevents');

Route::delete('/cancelreservation', 'EventController@cancelreservation');

//user--profile
Route::get('/makeprofile', 'ProfileController@create');
Route::patch('/makeprofile', 'ProfileController@store');
Route::get('/editprofile/{id}', 'ProfileController@edit');
Route::patch('/editprofile/{id}', 'ProfileController@update');

//user -recipe
Route::get('/addrecipe', 'RecipeController@create');
Route::post('/addrecipe', 'RecipeController@pls');
Route::get('/viewrecipe/{id}', 'RecipeController@show');
Route::get('/editrecipe/{id}', 'RecipeController@edit');
Route::patch('/editrecipe/{id}', 'RecipeController@update');
Route::delete('/deleterecipe/{id}', 'RecipeController@destroy');
});



Route::get('/viewevent/{id}', 'EventController@viewevent');

//user- profile
Route::get('/loggedin', 'ProfileController@loggedin');
Route::get('/profile', 'ProfileController@profile');
Route::post('/profile', 'ProfileController@filter');

Route::get('userallevents', 'EventController@userallevents');